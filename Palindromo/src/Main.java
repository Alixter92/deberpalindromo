import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //instaciamos la clase palindromo
        palindromo objclass = new palindromo();

        System.out.println("Ingrese el texto");

        //leemos el texto ingresado por consola
        Scanner sc = new Scanner(System.in);
        String cadena = sc.nextLine();

        //se inicia la funcion para determinar si es un palindromo
        if (objclass.espalindromo(cadena)) {

            System.out.println("palindromo");
        } else {
            System.out.println("No palindromo");
        }
    }
}