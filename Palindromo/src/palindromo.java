/**
 * Created by USER on 05/07/2017.
 */
public class palindromo {

    public boolean espalindromo(String cadena){
        boolean valor=true;
        int i,ind;
        String cadena2="";
        //quitamos los espacios en blanco si es una cadena larga
        for (int x=0; x < cadena.length(); x++) {
            if (cadena.charAt(x) != ' ')
                cadena2 += cadena.charAt(x);
        }

        cadena=cadena2;
        ind=cadena.length();
        //comparamo la cadena que se ingresa
        for (i= 0 ;i < (cadena.length()); i++){
            if (cadena.substring(i, i+1).equals(cadena.substring(ind - 1, ind)) == false ) {
                //si una sola letra no corresponde no es un palindromo por tanto
                //sale del ciclo con valor false
                valor=false;
                break;
            }
            ind--;
        }
        return valor;
    }
}
